$(document).ready(function(){
  $('.client_preferred_time').datetimepicker({
      datepicker:false,
      formatTime:"h:i a",
      step:30,
      format:"h:i a"
  });

  var promocodes = ['123456','123qwe','1a2s3d','qweasd'];
  $('#promoCodeModal form').submit(function(event) {
    event.preventDefault();
    let val = $('#promoCodeModal input').val();
    if($.inArray(val, promocodes) != -1){
      $(function () {
        $('#sucessModal').modal('show');
        $('#promoCodeModal').modal('toggle');
    });
    }else{
      $(function (){
        $('#expiredCoupon').modal('show');
        $('#promoCodeModal').modal('toggle');
      })
    }
  });

  $('#promoCodeModal').on('hidden.bs.modal', function (e) {
    $('#promoCodeModal form').trigger("reset");
    $('#promoCodeModal input').removeClass('is-invalid is-valid')
  })
});

$(document).ready(function () {

// Navbar and Footer     
    $('#navbar').load('layout/navbar.html');
    $('#footer').load('layout/footer.html');
    $('.carousel').carousel({
      interval: false
    });
  
}); var locations = [
  ['<p>Yo Compro Poblano</p><h3>Plaza Dorada</h3>', 19.028321847004474, -98.20283947889243, 3],
  ['<p>Yo Compro Poblano</p><h3>El Triángulo</h3>', 19.04371109965907, -98.23492258664703, 2],
  ['<p>Yo Compro Poblano</p><h3>Plaza Adagio</h3>', 18.993917, -98.276637, 1]
];

var map = new google.maps.Map(document.getElementById('map-container-google-1'), {
  zoom: 12,
  center: new google.maps.LatLng(30.052338, 31.235808),
  mapTypeId: google.maps.MapTypeId.ROADMAP
});

var infowindow = new google.maps.InfoWindow();

var marker, i;

for (i = 0; i < locations.length; i++) {  
  marker = new google.maps.Marker({
    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
    map: map
  });

  google.maps.event.addListener(marker, 'click', (function(marker, i) {
    return function() {
      infowindow.setContent(locations[i][0]);
      infowindow.open(map, marker);
    }
  })(marker, i));
}